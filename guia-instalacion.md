# GUÍA DE INSTALACIÓN DE UBUNTU MATE 18.04


A distribución Ubuntu MATE versión 18.04.2 está pensada para uso xeral aínda que se adapta ás necesidades específicas que teñen algúns dos equipos con menos recursos hardware, sobre todo no que se refire a necesidades gráficas.
Esta guía de instalación pretende ser unha axuda para o persoal técnico que só precisa ter en conta os pasos principais e os parámetros de configuración a escoller en cada momento.
Configuración inicial dun equipo informático

## Descarga e instalación
A distribución atópase no seguinte enderezo:

[ubuntu-mate-18.04.2-desktop-amd64.iso](http://cdimage.ubuntu.com/ubuntu-mate/releases/18.04/release/ubuntu-mate-18.04.2-desktop-amd64.iso)

Comezamos a instalación sen ningún requerimento especial, usando as opcións por defecto. Na sección de idioma, escollemos **Galego**.

![image](/images/instalacion_idioma.png)

Durante o proceso de instalación creamos o usuario por defecto do sistema que terá permisos para executar comandos con `sudo`. O nome será **administrador** co contrasinal que se vén usando ata agora (ma++++es).

![image](/images/instalacion_usuario.png)

## Instalación do cliente «migasfree»

Unha vez rematada a instalación do sistema operativo, a primeira operación a realizar será a de instalar o cliente de «migasfree» para que se poida conectar co servidor e realizar as tarefas programadas.
Abrimos un terminal e executamos a seguinte orde:

	$ wget -O - http://migasfree.org/pub/install-client | sudo bash

> **NOTA:** Cando se executa este comando hai que introducir o contrasinal do administrador para que se poida executar o script descargado.

A continuación, debemos engadir o nome da máquina onde se aloxa o servidor de «migasfree». Accedemos ao ficheiro `/etc/migasfree.conf` e modificamos a liña `Server`:

	$ sudo vi /etc/migasfree.conf
	[...]
	Server = scqmgf01
	[...]

Unha vez configurado o nome do servidor, xa podemos executar a primeira sincronización desde o cliente:

	$ sudo migasfree -u

A partir deste momento, o equipo cliente estará actualizado coas últimas versións do software instalado, e aparecerá no inventario de ordenadores no servidor de «migasfree».

## Configurar o despregue en Migasfree

Unha vez engadido o ordenador á base de datos do servidor Migasfree, temos que configurar un despregue para este novo ordenador. Podemos facer uso dalgún dos despregues activos, no que engadimos este ordenador na lista de equipos onde se executarán os cambios, ou ben, podemos crear un novo despregue, que será o que vexamos nesta guía.

Na seguinte captura vemos un resumo dos despregues do servidor divididos por proxecto:

![image](/images/migasfree-despregues.png)

Neste exemplo, visualizamos un despregue para o ordenador *ubuntu-bionic* que está integrado no proxecto Ubuntu 18.04. Neste despregue imos engadir os paquetes que deben figurar por defecto en todo ordenador instalado nas nosas dependencias. Primeiro hai que engadilos na lista de paquetes dispoñibles, e logo inserir o **nome do paquete** na caixa de *Paquetes a instalar*:

* scq-setup
* scq-mount
* scq-pbis

![image](/images/migasfree_novo-despregue.png)


## Engadir o equipo ao dominio CONCELLO

